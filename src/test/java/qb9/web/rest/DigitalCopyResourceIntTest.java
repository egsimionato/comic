package qb9.web.rest;

import qb9.ComicApp;
import qb9.domain.DigitalCopy;
import qb9.repository.DigitalCopyRepository;
import qb9.service.DigitalCopyService;
import qb9.web.rest.dto.DigitalCopyDTO;
import qb9.web.rest.mapper.DigitalCopyMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import qb9.domain.enumeration.CopyStatus;

/**
 * Test class for the DigitalCopyResource REST controller.
 *
 * @see DigitalCopyResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ComicApp.class)
@WebAppConfiguration
@IntegrationTest
public class DigitalCopyResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_UID = "AAAAA";
    private static final String UPDATED_UID = "BBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);

    private static final CopyStatus DEFAULT_STATUS = CopyStatus.ACTIVE;
    private static final CopyStatus UPDATED_STATUS = CopyStatus.DISABLED;
    private static final String DEFAULT_USERNAME = "AAAAA";
    private static final String UPDATED_USERNAME = "BBBBB";

    @Inject
    private DigitalCopyRepository digitalCopyRepository;

    @Inject
    private DigitalCopyMapper digitalCopyMapper;

    @Inject
    private DigitalCopyService digitalCopyService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restDigitalCopyMockMvc;

    private DigitalCopy digitalCopy;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        DigitalCopyResource digitalCopyResource = new DigitalCopyResource();
        ReflectionTestUtils.setField(digitalCopyResource, "digitalCopyService", digitalCopyService);
        ReflectionTestUtils.setField(digitalCopyResource, "digitalCopyMapper", digitalCopyMapper);
        this.restDigitalCopyMockMvc = MockMvcBuilders.standaloneSetup(digitalCopyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        digitalCopy = new DigitalCopy();
        digitalCopy.setUid(DEFAULT_UID);
        digitalCopy.setCreatedAt(DEFAULT_CREATED_AT);
        digitalCopy.setUpdatedAt(DEFAULT_UPDATED_AT);
        digitalCopy.setStatus(DEFAULT_STATUS);
        digitalCopy.setUsername(DEFAULT_USERNAME);
    }

    @Test
    @Transactional
    public void createDigitalCopy() throws Exception {
        int databaseSizeBeforeCreate = digitalCopyRepository.findAll().size();

        // Create the DigitalCopy
        DigitalCopyDTO digitalCopyDTO = digitalCopyMapper.digitalCopyToDigitalCopyDTO(digitalCopy);

        restDigitalCopyMockMvc.perform(post("/api/digital-copies")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(digitalCopyDTO)))
                .andExpect(status().isCreated());

        // Validate the DigitalCopy in the database
        List<DigitalCopy> digitalCopies = digitalCopyRepository.findAll();
        assertThat(digitalCopies).hasSize(databaseSizeBeforeCreate + 1);
        DigitalCopy testDigitalCopy = digitalCopies.get(digitalCopies.size() - 1);
        assertThat(testDigitalCopy.getUid()).isEqualTo(DEFAULT_UID);
        assertThat(testDigitalCopy.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testDigitalCopy.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testDigitalCopy.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testDigitalCopy.getUsername()).isEqualTo(DEFAULT_USERNAME);
    }

    @Test
    @Transactional
    public void getAllDigitalCopies() throws Exception {
        // Initialize the database
        digitalCopyRepository.saveAndFlush(digitalCopy);

        // Get all the digitalCopies
        restDigitalCopyMockMvc.perform(get("/api/digital-copies?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(digitalCopy.getId().intValue())))
                .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID.toString())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
                .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())));
    }

    @Test
    @Transactional
    public void getDigitalCopy() throws Exception {
        // Initialize the database
        digitalCopyRepository.saveAndFlush(digitalCopy);

        // Get the digitalCopy
        restDigitalCopyMockMvc.perform(get("/api/digital-copies/{id}", digitalCopy.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(digitalCopy.getId().intValue()))
            .andExpect(jsonPath("$.uid").value(DEFAULT_UID.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDigitalCopy() throws Exception {
        // Get the digitalCopy
        restDigitalCopyMockMvc.perform(get("/api/digital-copies/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDigitalCopy() throws Exception {
        // Initialize the database
        digitalCopyRepository.saveAndFlush(digitalCopy);
        int databaseSizeBeforeUpdate = digitalCopyRepository.findAll().size();

        // Update the digitalCopy
        DigitalCopy updatedDigitalCopy = new DigitalCopy();
        updatedDigitalCopy.setId(digitalCopy.getId());
        updatedDigitalCopy.setUid(UPDATED_UID);
        updatedDigitalCopy.setCreatedAt(UPDATED_CREATED_AT);
        updatedDigitalCopy.setUpdatedAt(UPDATED_UPDATED_AT);
        updatedDigitalCopy.setStatus(UPDATED_STATUS);
        updatedDigitalCopy.setUsername(UPDATED_USERNAME);
        DigitalCopyDTO digitalCopyDTO = digitalCopyMapper.digitalCopyToDigitalCopyDTO(updatedDigitalCopy);

        restDigitalCopyMockMvc.perform(put("/api/digital-copies")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(digitalCopyDTO)))
                .andExpect(status().isOk());

        // Validate the DigitalCopy in the database
        List<DigitalCopy> digitalCopies = digitalCopyRepository.findAll();
        assertThat(digitalCopies).hasSize(databaseSizeBeforeUpdate);
        DigitalCopy testDigitalCopy = digitalCopies.get(digitalCopies.size() - 1);
        assertThat(testDigitalCopy.getUid()).isEqualTo(UPDATED_UID);
        assertThat(testDigitalCopy.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testDigitalCopy.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testDigitalCopy.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDigitalCopy.getUsername()).isEqualTo(UPDATED_USERNAME);
    }

    @Test
    @Transactional
    public void deleteDigitalCopy() throws Exception {
        // Initialize the database
        digitalCopyRepository.saveAndFlush(digitalCopy);
        int databaseSizeBeforeDelete = digitalCopyRepository.findAll().size();

        // Get the digitalCopy
        restDigitalCopyMockMvc.perform(delete("/api/digital-copies/{id}", digitalCopy.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<DigitalCopy> digitalCopies = digitalCopyRepository.findAll();
        assertThat(digitalCopies).hasSize(databaseSizeBeforeDelete - 1);
    }
}
