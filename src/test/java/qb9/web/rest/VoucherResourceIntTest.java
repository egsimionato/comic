package qb9.web.rest;

import qb9.ComicApp;
import qb9.domain.Voucher;
import qb9.repository.VoucherRepository;
import qb9.service.VoucherService;
import qb9.web.rest.dto.VoucherDTO;
import qb9.web.rest.mapper.VoucherMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import qb9.domain.enumeration.VoucherType;
import qb9.domain.enumeration.VoucherStatus;

/**
 * Test class for the VoucherResource REST controller.
 *
 * @see VoucherResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ComicApp.class)
@WebAppConfiguration
@IntegrationTest
public class VoucherResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_UID = "AAAAA";
    private static final String UPDATED_UID = "BBBBB";

    private static final VoucherType DEFAULT_TYPE = VoucherType.BUY;
    private static final VoucherType UPDATED_TYPE = VoucherType.FREE;

    private static final ZonedDateTime DEFAULT_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATE_DATE_STR = dateTimeFormatter.format(DEFAULT_CREATE_DATE);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);

    private static final VoucherStatus DEFAULT_STATUS = VoucherStatus.NEW;
    private static final VoucherStatus UPDATED_STATUS = VoucherStatus.REDEEMED;

    @Inject
    private VoucherRepository voucherRepository;

    @Inject
    private VoucherMapper voucherMapper;

    @Inject
    private VoucherService voucherService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restVoucherMockMvc;

    private Voucher voucher;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        VoucherResource voucherResource = new VoucherResource();
        ReflectionTestUtils.setField(voucherResource, "voucherService", voucherService);
        ReflectionTestUtils.setField(voucherResource, "voucherMapper", voucherMapper);
        this.restVoucherMockMvc = MockMvcBuilders.standaloneSetup(voucherResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        voucher = new Voucher();
        voucher.setUid(DEFAULT_UID);
        voucher.setType(DEFAULT_TYPE);
        voucher.setCreateDate(DEFAULT_CREATE_DATE);
        voucher.setUpdatedAt(DEFAULT_UPDATED_AT);
        voucher.setStatus(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createVoucher() throws Exception {
        int databaseSizeBeforeCreate = voucherRepository.findAll().size();

        // Create the Voucher
        VoucherDTO voucherDTO = voucherMapper.voucherToVoucherDTO(voucher);

        restVoucherMockMvc.perform(post("/api/vouchers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(voucherDTO)))
                .andExpect(status().isCreated());

        // Validate the Voucher in the database
        List<Voucher> vouchers = voucherRepository.findAll();
        assertThat(vouchers).hasSize(databaseSizeBeforeCreate + 1);
        Voucher testVoucher = vouchers.get(vouchers.size() - 1);
        assertThat(testVoucher.getUid()).isEqualTo(DEFAULT_UID);
        assertThat(testVoucher.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testVoucher.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testVoucher.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testVoucher.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllVouchers() throws Exception {
        // Initialize the database
        voucherRepository.saveAndFlush(voucher);

        // Get all the vouchers
        restVoucherMockMvc.perform(get("/api/vouchers?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(voucher.getId().intValue())))
                .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID.toString())))
                .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
                .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getVoucher() throws Exception {
        // Initialize the database
        voucherRepository.saveAndFlush(voucher);

        // Get the voucher
        restVoucherMockMvc.perform(get("/api/vouchers/{id}", voucher.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(voucher.getId().intValue()))
            .andExpect(jsonPath("$.uid").value(DEFAULT_UID.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingVoucher() throws Exception {
        // Get the voucher
        restVoucherMockMvc.perform(get("/api/vouchers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVoucher() throws Exception {
        // Initialize the database
        voucherRepository.saveAndFlush(voucher);
        int databaseSizeBeforeUpdate = voucherRepository.findAll().size();

        // Update the voucher
        Voucher updatedVoucher = new Voucher();
        updatedVoucher.setId(voucher.getId());
        updatedVoucher.setUid(UPDATED_UID);
        updatedVoucher.setType(UPDATED_TYPE);
        updatedVoucher.setCreateDate(UPDATED_CREATE_DATE);
        updatedVoucher.setUpdatedAt(UPDATED_UPDATED_AT);
        updatedVoucher.setStatus(UPDATED_STATUS);
        VoucherDTO voucherDTO = voucherMapper.voucherToVoucherDTO(updatedVoucher);

        restVoucherMockMvc.perform(put("/api/vouchers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(voucherDTO)))
                .andExpect(status().isOk());

        // Validate the Voucher in the database
        List<Voucher> vouchers = voucherRepository.findAll();
        assertThat(vouchers).hasSize(databaseSizeBeforeUpdate);
        Voucher testVoucher = vouchers.get(vouchers.size() - 1);
        assertThat(testVoucher.getUid()).isEqualTo(UPDATED_UID);
        assertThat(testVoucher.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testVoucher.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testVoucher.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testVoucher.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deleteVoucher() throws Exception {
        // Initialize the database
        voucherRepository.saveAndFlush(voucher);
        int databaseSizeBeforeDelete = voucherRepository.findAll().size();

        // Get the voucher
        restVoucherMockMvc.perform(delete("/api/vouchers/{id}", voucher.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Voucher> vouchers = voucherRepository.findAll();
        assertThat(vouchers).hasSize(databaseSizeBeforeDelete - 1);
    }
}
