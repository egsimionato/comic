package qb9.web.rest;

import qb9.ComicApp;
import qb9.domain.Account;
import qb9.repository.AccountRepository;
import qb9.service.AccountService;
import qb9.web.rest.dto.AccountDTO;
import qb9.web.rest.mapper.AccountMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import qb9.domain.enumeration.AccountStatus;

/**
 * Test class for the AccountResource REST controller.
 *
 * @see AccountResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ComicApp.class)
@WebAppConfiguration
@IntegrationTest
public class AccountResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_UID = "AAAAA";
    private static final String UPDATED_UID = "BBBBB";
    private static final String DEFAULT_EMAIL = "AAAAA";
    private static final String UPDATED_EMAIL = "BBBBB";
    private static final String DEFAULT_USERNAME = "AAAAA";
    private static final String UPDATED_USERNAME = "BBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);

    private static final AccountStatus DEFAULT_STATUS = AccountStatus.NEW;
    private static final AccountStatus UPDATED_STATUS = AccountStatus.ACTIVE;

    @Inject
    private AccountRepository accountRepository;

    @Inject
    private AccountMapper accountMapper;

    @Inject
    private AccountService accountService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restAccountMockMvc;

    private Account account;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        AccountResource accountResource = new AccountResource();
        ReflectionTestUtils.setField(accountResource, "accountService", accountService);
        ReflectionTestUtils.setField(accountResource, "accountMapper", accountMapper);
        this.restAccountMockMvc = MockMvcBuilders.standaloneSetup(accountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        account = new Account();
        account.setUid(DEFAULT_UID);
        account.setEmail(DEFAULT_EMAIL);
        account.setUsername(DEFAULT_USERNAME);
        account.setCreatedAt(DEFAULT_CREATED_AT);
        account.setUpdatedAt(DEFAULT_UPDATED_AT);
        account.setStatus(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createAccount() throws Exception {
        int databaseSizeBeforeCreate = accountRepository.findAll().size();

        // Create the Account
        AccountDTO accountDTO = accountMapper.accountToAccountDTO(account);

        restAccountMockMvc.perform(post("/api/accounts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(accountDTO)))
                .andExpect(status().isCreated());

        // Validate the Account in the database
        List<Account> accounts = accountRepository.findAll();
        assertThat(accounts).hasSize(databaseSizeBeforeCreate + 1);
        Account testAccount = accounts.get(accounts.size() - 1);
        assertThat(testAccount.getUid()).isEqualTo(DEFAULT_UID);
        assertThat(testAccount.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testAccount.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testAccount.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testAccount.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testAccount.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllAccounts() throws Exception {
        // Initialize the database
        accountRepository.saveAndFlush(account);

        // Get all the accounts
        restAccountMockMvc.perform(get("/api/accounts?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(account.getId().intValue())))
                .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID.toString())))
                .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
                .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME.toString())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getAccount() throws Exception {
        // Initialize the database
        accountRepository.saveAndFlush(account);

        // Get the account
        restAccountMockMvc.perform(get("/api/accounts/{id}", account.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(account.getId().intValue()))
            .andExpect(jsonPath("$.uid").value(DEFAULT_UID.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAccount() throws Exception {
        // Get the account
        restAccountMockMvc.perform(get("/api/accounts/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAccount() throws Exception {
        // Initialize the database
        accountRepository.saveAndFlush(account);
        int databaseSizeBeforeUpdate = accountRepository.findAll().size();

        // Update the account
        Account updatedAccount = new Account();
        updatedAccount.setId(account.getId());
        updatedAccount.setUid(UPDATED_UID);
        updatedAccount.setEmail(UPDATED_EMAIL);
        updatedAccount.setUsername(UPDATED_USERNAME);
        updatedAccount.setCreatedAt(UPDATED_CREATED_AT);
        updatedAccount.setUpdatedAt(UPDATED_UPDATED_AT);
        updatedAccount.setStatus(UPDATED_STATUS);
        AccountDTO accountDTO = accountMapper.accountToAccountDTO(updatedAccount);

        restAccountMockMvc.perform(put("/api/accounts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(accountDTO)))
                .andExpect(status().isOk());

        // Validate the Account in the database
        List<Account> accounts = accountRepository.findAll();
        assertThat(accounts).hasSize(databaseSizeBeforeUpdate);
        Account testAccount = accounts.get(accounts.size() - 1);
        assertThat(testAccount.getUid()).isEqualTo(UPDATED_UID);
        assertThat(testAccount.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAccount.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testAccount.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testAccount.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testAccount.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deleteAccount() throws Exception {
        // Initialize the database
        accountRepository.saveAndFlush(account);
        int databaseSizeBeforeDelete = accountRepository.findAll().size();

        // Get the account
        restAccountMockMvc.perform(delete("/api/accounts/{id}", account.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Account> accounts = accountRepository.findAll();
        assertThat(accounts).hasSize(databaseSizeBeforeDelete - 1);
    }
}
