package qb9.web.rest;

import qb9.ComicApp;
import qb9.domain.Comic;
import qb9.repository.ComicRepository;
import qb9.service.ComicService;
import qb9.web.rest.dto.ComicDTO;
import qb9.web.rest.mapper.ComicMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import qb9.domain.enumeration.ComicStatus;

/**
 * Test class for the ComicResource REST controller.
 *
 * @see ComicResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ComicApp.class)
@WebAppConfiguration
@IntegrationTest
public class ComicResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_UID = "AAAAA";
    private static final String UPDATED_UID = "BBBBB";
    private static final String DEFAULT_CODE = "AAAAA";
    private static final String UPDATED_CODE = "BBBBB";
    private static final String DEFAULT_SUMMARY = "AAAAA";
    private static final String UPDATED_SUMMARY = "BBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);
    private static final String DEFAULT_TITLE = "AAAAA";
    private static final String UPDATED_TITLE = "BBBBB";
    private static final String DEFAULT_SAFE_TITLE = "AAAAA";
    private static final String UPDATED_SAFE_TITLE = "BBBBB";

    private static final Integer DEFAULT_NUMBER = 1;
    private static final Integer UPDATED_NUMBER = 2;
    private static final String DEFAULT_IMAGE_SRC = "AAAAA";
    private static final String UPDATED_IMAGE_SRC = "BBBBB";

    private static final ComicStatus DEFAULT_STATUS = ComicStatus.DRAFT;
    private static final ComicStatus UPDATED_STATUS = ComicStatus.NEW;

    @Inject
    private ComicRepository comicRepository;

    @Inject
    private ComicMapper comicMapper;

    @Inject
    private ComicService comicService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restComicMockMvc;

    private Comic comic;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ComicResource comicResource = new ComicResource();
        ReflectionTestUtils.setField(comicResource, "comicService", comicService);
        ReflectionTestUtils.setField(comicResource, "comicMapper", comicMapper);
        this.restComicMockMvc = MockMvcBuilders.standaloneSetup(comicResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        comic = new Comic();
        comic.setUid(DEFAULT_UID);
        comic.setCode(DEFAULT_CODE);
        comic.setSummary(DEFAULT_SUMMARY);
        comic.setCreatedAt(DEFAULT_CREATED_AT);
        comic.setUpdatedAt(DEFAULT_UPDATED_AT);
        comic.setTitle(DEFAULT_TITLE);
        comic.setSafeTitle(DEFAULT_SAFE_TITLE);
        comic.setNumber(DEFAULT_NUMBER);
        comic.setImageSrc(DEFAULT_IMAGE_SRC);
        comic.setStatus(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createComic() throws Exception {
        int databaseSizeBeforeCreate = comicRepository.findAll().size();

        // Create the Comic
        ComicDTO comicDTO = comicMapper.comicToComicDTO(comic);

        restComicMockMvc.perform(post("/api/comics")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(comicDTO)))
                .andExpect(status().isCreated());

        // Validate the Comic in the database
        List<Comic> comics = comicRepository.findAll();
        assertThat(comics).hasSize(databaseSizeBeforeCreate + 1);
        Comic testComic = comics.get(comics.size() - 1);
        assertThat(testComic.getUid()).isEqualTo(DEFAULT_UID);
        assertThat(testComic.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testComic.getSummary()).isEqualTo(DEFAULT_SUMMARY);
        assertThat(testComic.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testComic.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testComic.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testComic.getSafeTitle()).isEqualTo(DEFAULT_SAFE_TITLE);
        assertThat(testComic.getNumber()).isEqualTo(DEFAULT_NUMBER);
        assertThat(testComic.getImageSrc()).isEqualTo(DEFAULT_IMAGE_SRC);
        assertThat(testComic.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllComics() throws Exception {
        // Initialize the database
        comicRepository.saveAndFlush(comic);

        // Get all the comics
        restComicMockMvc.perform(get("/api/comics?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(comic.getId().intValue())))
                .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID.toString())))
                .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
                .andExpect(jsonPath("$.[*].summary").value(hasItem(DEFAULT_SUMMARY.toString())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)))
                .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
                .andExpect(jsonPath("$.[*].safeTitle").value(hasItem(DEFAULT_SAFE_TITLE.toString())))
                .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER)))
                .andExpect(jsonPath("$.[*].imageSrc").value(hasItem(DEFAULT_IMAGE_SRC.toString())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getComic() throws Exception {
        // Initialize the database
        comicRepository.saveAndFlush(comic);

        // Get the comic
        restComicMockMvc.perform(get("/api/comics/{id}", comic.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(comic.getId().intValue()))
            .andExpect(jsonPath("$.uid").value(DEFAULT_UID.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.summary").value(DEFAULT_SUMMARY.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.safeTitle").value(DEFAULT_SAFE_TITLE.toString()))
            .andExpect(jsonPath("$.number").value(DEFAULT_NUMBER))
            .andExpect(jsonPath("$.imageSrc").value(DEFAULT_IMAGE_SRC.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingComic() throws Exception {
        // Get the comic
        restComicMockMvc.perform(get("/api/comics/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateComic() throws Exception {
        // Initialize the database
        comicRepository.saveAndFlush(comic);
        int databaseSizeBeforeUpdate = comicRepository.findAll().size();

        // Update the comic
        Comic updatedComic = new Comic();
        updatedComic.setId(comic.getId());
        updatedComic.setUid(UPDATED_UID);
        updatedComic.setCode(UPDATED_CODE);
        updatedComic.setSummary(UPDATED_SUMMARY);
        updatedComic.setCreatedAt(UPDATED_CREATED_AT);
        updatedComic.setUpdatedAt(UPDATED_UPDATED_AT);
        updatedComic.setTitle(UPDATED_TITLE);
        updatedComic.setSafeTitle(UPDATED_SAFE_TITLE);
        updatedComic.setNumber(UPDATED_NUMBER);
        updatedComic.setImageSrc(UPDATED_IMAGE_SRC);
        updatedComic.setStatus(UPDATED_STATUS);
        ComicDTO comicDTO = comicMapper.comicToComicDTO(updatedComic);

        restComicMockMvc.perform(put("/api/comics")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(comicDTO)))
                .andExpect(status().isOk());

        // Validate the Comic in the database
        List<Comic> comics = comicRepository.findAll();
        assertThat(comics).hasSize(databaseSizeBeforeUpdate);
        Comic testComic = comics.get(comics.size() - 1);
        assertThat(testComic.getUid()).isEqualTo(UPDATED_UID);
        assertThat(testComic.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testComic.getSummary()).isEqualTo(UPDATED_SUMMARY);
        assertThat(testComic.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testComic.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testComic.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testComic.getSafeTitle()).isEqualTo(UPDATED_SAFE_TITLE);
        assertThat(testComic.getNumber()).isEqualTo(UPDATED_NUMBER);
        assertThat(testComic.getImageSrc()).isEqualTo(UPDATED_IMAGE_SRC);
        assertThat(testComic.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deleteComic() throws Exception {
        // Initialize the database
        comicRepository.saveAndFlush(comic);
        int databaseSizeBeforeDelete = comicRepository.findAll().size();

        // Get the comic
        restComicMockMvc.perform(delete("/api/comics/{id}", comic.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Comic> comics = comicRepository.findAll();
        assertThat(comics).hasSize(databaseSizeBeforeDelete - 1);
    }
}
