package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.domain.DigitalCopy;
import qb9.service.DigitalCopyService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.web.rest.dto.DigitalCopyDTO;
import qb9.web.rest.mapper.DigitalCopyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing DigitalCopy.
 */
@RestController
@RequestMapping("/api")
public class DigitalCopyResource {

    private final Logger log = LoggerFactory.getLogger(DigitalCopyResource.class);

    @Inject
    private DigitalCopyService digitalCopyService;

    @Inject
    private DigitalCopyMapper digitalCopyMapper;

    /**
     * POST  /digital-copies : Create a new digitalCopy.
     *
     * @param digitalCopyDTO the digitalCopyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new digitalCopyDTO, or with status 400 (Bad Request) if the digitalCopy has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/digital-copies",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DigitalCopyDTO> createDigitalCopy(@RequestBody DigitalCopyDTO digitalCopyDTO) throws URISyntaxException {
        log.debug("REST request to save DigitalCopy : {}", digitalCopyDTO);
        if (digitalCopyDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("digitalCopy", "idexists", "A new digitalCopy cannot already have an ID")).body(null);
        }
        DigitalCopyDTO result = digitalCopyService.save(digitalCopyDTO);
        return ResponseEntity.created(new URI("/api/digital-copies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("digitalCopy", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /digital-copies : Updates an existing digitalCopy.
     *
     * @param digitalCopyDTO the digitalCopyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated digitalCopyDTO,
     * or with status 400 (Bad Request) if the digitalCopyDTO is not valid,
     * or with status 500 (Internal Server Error) if the digitalCopyDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/digital-copies",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DigitalCopyDTO> updateDigitalCopy(@RequestBody DigitalCopyDTO digitalCopyDTO) throws URISyntaxException {
        log.debug("REST request to update DigitalCopy : {}", digitalCopyDTO);
        if (digitalCopyDTO.getId() == null) {
            return createDigitalCopy(digitalCopyDTO);
        }
        DigitalCopyDTO result = digitalCopyService.save(digitalCopyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("digitalCopy", digitalCopyDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /digital-copies : get all the digitalCopies.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of digitalCopies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/digital-copies",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<DigitalCopyDTO>> getAllDigitalCopies(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of DigitalCopies");
        Page<DigitalCopy> page = digitalCopyService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/digital-copies");
        return new ResponseEntity<>(digitalCopyMapper.digitalCopiesToDigitalCopyDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /digital-copies/:id : get the "id" digitalCopy.
     *
     * @param id the id of the digitalCopyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the digitalCopyDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/digital-copies/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<DigitalCopyDTO> getDigitalCopy(@PathVariable Long id) {
        log.debug("REST request to get DigitalCopy : {}", id);
        DigitalCopyDTO digitalCopyDTO = digitalCopyService.findOne(id);
        return Optional.ofNullable(digitalCopyDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /digital-copies/:id : delete the "id" digitalCopy.
     *
     * @param id the id of the digitalCopyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/digital-copies/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteDigitalCopy(@PathVariable Long id) {
        log.debug("REST request to delete DigitalCopy : {}", id);
        digitalCopyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("digitalCopy", id.toString())).build();
    }


    /**
     * GET  /digital-copies : get all the digitalCopies.
     *
     * @param username the username of the digitalCopy to find
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of digitalCopies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/digital-copies/by_username/{username}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<DigitalCopyDTO>> getDigitalCopiesByUsername(Pageable pageable, @PathVariable String username)
        throws URISyntaxException {
        log.debug("REST request to get a page of DigitalCopies By Username");
        Page<DigitalCopy> page = digitalCopyService.findByUsername(username, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/digital-copies");
        return new ResponseEntity<>(digitalCopyMapper.digitalCopiesToDigitalCopyDTOs(page.getContent()), headers, HttpStatus.OK);
    }

}
