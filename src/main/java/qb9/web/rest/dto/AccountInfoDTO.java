package qb9.web.rest.dto;

import qb9.config.Constants;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 * A DTO representing the account info response.
 */
public class AccountInfoDTO {

    private Long id;

    private String uid;

    private String username;

    private ZonedDateTime lastFreedAt;

    private ZonedDateTime comingFreedAt;

    private Long vouchersAvailableQty;

    private Integer voucherPrice;

    private Long freeVoucherElapsedTimeInSeconds;


    public Long getId() {
        return id;
    }

    public AccountInfoDTO setId(Long id) {
        this.id = id;
        return this;
    }
    public String getUid() {
        return uid;
    }

    public AccountInfoDTO setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public AccountInfoDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public ZonedDateTime getLastFreedAt() {
        return lastFreedAt;
    }

    public AccountInfoDTO setLastFreedAt(ZonedDateTime lastFreedAt) {
        this.lastFreedAt = lastFreedAt;
        return this;
    }


    public ZonedDateTime getComingFreedAt() {
        return comingFreedAt;
    }

    public AccountInfoDTO setComingFreedAt(ZonedDateTime comingFreedAt) {
        this.comingFreedAt = comingFreedAt;
        return this;
    }



    public Long getVouchersAvailableQty() {
        return vouchersAvailableQty;
    }

    public AccountInfoDTO setVouchersAvailableQty(Long qty) {
        this.vouchersAvailableQty = qty;
        return this;
    }


    public Integer getVoucherPrice() {
        return voucherPrice;
    }

    public AccountInfoDTO setVoucherPrice(Integer price) {
        this.voucherPrice = price;
        return this;
    }


    public Long getFreeVoucherElapsedTimeInSeconds() {
        return freeVoucherElapsedTimeInSeconds;
    }

    public AccountInfoDTO setFreeVoucherElapsedTimeInSeconds(Long elapsedTimeInSeconds) {
        this.freeVoucherElapsedTimeInSeconds = elapsedTimeInSeconds;
        return this;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AccountInfoDTO accountInfoDTO = (AccountInfoDTO) o;

        if ( ! Objects.equals(id, accountInfoDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "AccountInfoDTO{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", username='" + username + "'" +
            ", lastFreedAt='" + lastFreedAt + "'" +
            ", comingFreedAt='" + comingFreedAt + "'" +
            ", vouchersAvailableQty='" + vouchersAvailableQty + "'" +
            ", voucherPrice='" + voucherPrice + "'" +
            ", freeVoucherElapsedTimeInSeconds='" + freeVoucherElapsedTimeInSeconds + "'" +
            '}';
    }
}
