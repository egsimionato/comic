package qb9.web.rest.dto;

import qb9.config.Constants;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

/**
 * A DTO representing a unlock request.
 */
public class UnlockRandomDTO {

    @NotNull
    @Size(min = 1, max = 50)
    private String username;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @Override
    public String toString() {
        return "UnlockComicDTO{" +
            "username='" + username + '\'' +
            '}';
    }
}
