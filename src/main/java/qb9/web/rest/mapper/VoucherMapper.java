package qb9.web.rest.mapper;

import qb9.domain.*;
import qb9.web.rest.dto.VoucherDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Voucher and its DTO VoucherDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface VoucherMapper {

    @Mapping(source = "account.id", target = "accountId")
    @Mapping(source = "account.username", target = "accountUsername")
    VoucherDTO voucherToVoucherDTO(Voucher voucher);

    List<VoucherDTO> vouchersToVoucherDTOs(List<Voucher> vouchers);

    @Mapping(source = "accountId", target = "account")
    @Mapping(target = "digitalCopy", ignore = true)
    Voucher voucherDTOToVoucher(VoucherDTO voucherDTO);

    List<Voucher> voucherDTOsToVouchers(List<VoucherDTO> voucherDTOs);

    default Account accountFromId(Long id) {
        if (id == null) {
            return null;
        }
        Account account = new Account();
        account.setId(id);
        return account;
    }
}
