package qb9.web.rest.mapper;

import qb9.domain.*;
import qb9.web.rest.dto.DigitalCopyDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity DigitalCopy and its DTO DigitalCopyDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DigitalCopyMapper {

    @Mapping(source = "comic.id", target = "comicId")
    @Mapping(source = "comic.code", target = "comicCode")
    @Mapping(source = "account.id", target = "accountId")
    @Mapping(source = "account.username", target = "accountUsername")
    @Mapping(source = "voucher.id", target = "voucherId")
    DigitalCopyDTO digitalCopyToDigitalCopyDTO(DigitalCopy digitalCopy);

    List<DigitalCopyDTO> digitalCopiesToDigitalCopyDTOs(List<DigitalCopy> digitalCopies);

    @Mapping(source = "comicId", target = "comic")
    @Mapping(source = "accountId", target = "account")
    @Mapping(source = "voucherId", target = "voucher")
    DigitalCopy digitalCopyDTOToDigitalCopy(DigitalCopyDTO digitalCopyDTO);

    List<DigitalCopy> digitalCopyDTOsToDigitalCopies(List<DigitalCopyDTO> digitalCopyDTOs);

    default Comic comicFromId(Long id) {
        if (id == null) {
            return null;
        }
        Comic comic = new Comic();
        comic.setId(id);
        return comic;
    }

    default Account accountFromId(Long id) {
        if (id == null) {
            return null;
        }
        Account account = new Account();
        account.setId(id);
        return account;
    }

    default Voucher voucherFromId(Long id) {
        if (id == null) {
            return null;
        }
        Voucher voucher = new Voucher();
        voucher.setId(id);
        return voucher;
    }
}
