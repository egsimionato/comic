package qb9.web.rest.mapper;

import qb9.domain.*;
import qb9.web.rest.dto.ComicDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Comic and its DTO ComicDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ComicMapper {

    ComicDTO comicToComicDTO(Comic comic);

    List<ComicDTO> comicsToComicDTOs(List<Comic> comics);

    Comic comicDTOToComic(ComicDTO comicDTO);

    List<Comic> comicDTOsToComics(List<ComicDTO> comicDTOs);
}
