package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.domain.Voucher;
import qb9.domain.enumeration.VoucherStatus;
import qb9.repository.VoucherRepository;
import qb9.service.VoucherService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.web.rest.dto.VoucherDTO;
import qb9.web.rest.mapper.VoucherMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing Voucher.
 */
@RestController
@RequestMapping("/api")
public class VoucherResource {

    private final Logger log = LoggerFactory.getLogger(VoucherResource.class);

    @Inject
    private VoucherService voucherService;

    @Inject
    private VoucherRepository voucherRepository;

    @Inject
    private VoucherMapper voucherMapper;

    /**
     * POST  /vouchers : Create a new voucher.
     *
     * @param voucherDTO the voucherDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new voucherDTO, or with status 400 (Bad Request) if the voucher has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/vouchers",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<VoucherDTO> createVoucher(@RequestBody VoucherDTO voucherDTO) throws URISyntaxException {
        log.debug("REST request to save Voucher : {}", voucherDTO);
        if (voucherDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("voucher", "idexists", "A new voucher cannot already have an ID")).body(null);
        }
        VoucherDTO result = voucherService.save(voucherDTO);
        return ResponseEntity.created(new URI("/api/vouchers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("voucher", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /vouchers : Updates an existing voucher.
     *
     * @param voucherDTO the voucherDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated voucherDTO,
     * or with status 400 (Bad Request) if the voucherDTO is not valid,
     * or with status 500 (Internal Server Error) if the voucherDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/vouchers",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<VoucherDTO> updateVoucher(@RequestBody VoucherDTO voucherDTO) throws URISyntaxException {
        log.debug("REST request to update Voucher : {}", voucherDTO);
        if (voucherDTO.getId() == null) {
            return createVoucher(voucherDTO);
        }
        VoucherDTO result = voucherService.save(voucherDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("voucher", voucherDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /vouchers : get all the vouchers.
     *
     * @param pageable the pagination information
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of vouchers in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/vouchers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<VoucherDTO>> getAllVouchers(Pageable pageable, @RequestParam(required = false) String filter)
        throws URISyntaxException {
        if ("digitalcopy-is-null".equals(filter)) {
            log.debug("REST request to get all Vouchers where digitalCopy is null");
            return new ResponseEntity<>(voucherService.findAllWhereDigitalCopyIsNull(),
                    HttpStatus.OK);
        }
        log.debug("REST request to get a page of Vouchers");
        Page<Voucher> page = voucherService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vouchers");
        return new ResponseEntity<>(voucherMapper.vouchersToVoucherDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /vouchers/:id : get the "id" voucher.
     *
     * @param id the id of the voucherDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the voucherDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/vouchers/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<VoucherDTO> getVoucher(@PathVariable Long id) {
        log.debug("REST request to get Voucher : {}", id);

        VoucherDTO voucherDTO = voucherService.findOne(id);
        return Optional.ofNullable(voucherDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /vouchers/:id : delete the "id" voucher.
     *
     * @param id the id of the voucherDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/vouchers/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteVoucher(@PathVariable Long id) {
        log.debug("REST request to delete Voucher : {}", id);
        voucherService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("voucher", id.toString())).build();
    }

    /**
     * GET  /vouchers : get by username.
     *
     * @param username the username of the digitalCopy to find
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of digitalCopies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/vouchers/by_username/{username}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<VoucherDTO>> getVouchersByUsername(Pageable pageable, @PathVariable String username)
        throws URISyntaxException {
        log.debug("REST request to get a page of Vouchers By Username");
        Page<Voucher> page = voucherService.findByUsername(username, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vouchers");
        return new ResponseEntity<>(voucherMapper.vouchersToVoucherDTOs(page.getContent()), headers, HttpStatus.OK);
    }


}
