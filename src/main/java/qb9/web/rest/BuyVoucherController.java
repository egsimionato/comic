package qb9.web.rest;

import qb9.web.rest.dto.BuyVoucherDTO;
import qb9.web.rest.dto.AccountInfoDTO;

import com.codahale.metrics.annotation.Timed;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.math.BigDecimal;
import javax.validation.Valid;
import javax.servlet.http.HttpServletResponse;

import io.swagger.annotations.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;



@Component
@RestController
@Transactional
public class BuyVoucherController {

    private final Logger log = LoggerFactory.getLogger(BuyVoucherController.class);

    /**
     * POST  account/{username}/buy_voucher : Buy a comic voucher by a player.
     *
     * @param rewardDTO the reward info to processed.
     * @return the ResponseEntity with status 200 (Created) and with body the AccountInfoDTO information,
     * or with status 400 (Bad Request) if the player or goal are not registred,
     * or status 500 (Internal Server Error) if the reward couldn't be done.
     *
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "Buy a voucher",
            nickname = "buy_voucher",
            notes = "Buy a Voucher by a player")
    @RequestMapping(value = "/api/buy_voucher", method = RequestMethod.POST)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = AccountInfoDTO.class),
        @ApiResponse(code = 400, message = "Invalid params"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Internal Server Error")})
    @Timed
    public ResponseEntity<?> buyVoucher(
            @ApiParam(value="the buy info to processed.", required=true) @Valid @RequestBody BuyVoucherDTO buyDTO,
            @ApiParam(value="enable debug.", required=false) @RequestParam(value="debug", required=false) Optional<Boolean> isDebugEnabled,
            HttpServletResponse response) {
        log.debug("REST request to buy a voucher");

        AccountInfoDTO accInfoDTO =  new AccountInfoDTO();
        accInfoDTO.setUsername(buyDTO.getUsername())
            .setVoucherPrice(20)
            .setFreeVoucherElapsedTimeInSeconds(50l);
        return new ResponseEntity<>(accInfoDTO, HttpStatus.OK);

    }
}
