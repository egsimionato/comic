package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.domain.Comic;
import qb9.domain.enumeration.ComicStatus;
import qb9.service.ComicService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.web.rest.dto.ComicDTO;
import qb9.web.rest.mapper.ComicMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Comic.
 */
@RestController
@RequestMapping("/api")
public class ComicResource {

    private final Logger log = LoggerFactory.getLogger(ComicResource.class);

    @Inject
    private ComicService comicService;

    @Inject
    private ComicMapper comicMapper;

    /**
     * POST  /comics : Create a new comic.
     *
     * @param comicDTO the comicDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new comicDTO, or with status 400 (Bad Request) if the comic has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/comics",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ComicDTO> createComic(@RequestBody ComicDTO comicDTO) throws URISyntaxException {
        log.debug("REST request to save Comic : {}", comicDTO);
        if (comicDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("comic", "idexists", "A new comic cannot already have an ID")).body(null);
        }
        comicDTO.setStatus(ComicStatus.NEW);
        ComicDTO result = comicService.save(comicDTO);
        return ResponseEntity.created(new URI("/api/comics/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("comic", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /comics : Updates an existing comic.
     *
     * @param comicDTO the comicDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated comicDTO,
     * or with status 400 (Bad Request) if the comicDTO is not valid,
     * or with status 500 (Internal Server Error) if the comicDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/comics",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ComicDTO> updateComic(@RequestBody ComicDTO comicDTO) throws URISyntaxException {
        log.debug("REST request to update Comic : {}", comicDTO);
        if (comicDTO.getId() == null) {
            return createComic(comicDTO);
        }
        ComicDTO result = comicService.save(comicDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("comic", comicDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /comics : get all the comics.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of comics in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/comics",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<ComicDTO>> getAllComics(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Comics");
        Page<Comic> page = comicService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/comics");
        return new ResponseEntity<>(comicMapper.comicsToComicDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /comics/:id : get the "id" comic.
     *
     * @param id the id of the comicDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the comicDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/comics/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ComicDTO> getComic(@PathVariable Long id) {
        log.debug("REST request to get Comic : {}", id);
        ComicDTO comicDTO = comicService.findOne(id);
        return Optional.ofNullable(comicDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /comics/:id : delete the "id" comic.
     *
     * @param id the id of the comicDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/comics/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteComic(@PathVariable Long id) {
        log.debug("REST request to delete Comic : {}", id);
        comicService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("comic", id.toString())).build();
    }

}
