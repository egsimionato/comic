package qb9.web.rest;

import qb9.domain.Account;
import qb9.domain.Comic;
import qb9.domain.DigitalCopy;
import qb9.domain.enumeration.ComicStatus;
import qb9.repository.AccountRepository;
import qb9.repository.ComicRepository;
import qb9.service.UnlockComicService;
import qb9.web.rest.dto.ComicDTO;
import qb9.web.rest.dto.UnlockComicDTO;
import qb9.web.rest.dto.UnlockRandomDTO;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.errors.ErrorDTO;
import qb9.web.rest.errors.ErrorConstants;

import com.codahale.metrics.annotation.Timed;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import io.swagger.annotations.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;



@Component
@RestController
@Transactional
public class UnlockComicController {

    private final Logger log = LoggerFactory.getLogger(UnlockComicController.class);

    @Inject
    private ComicRepository comicRepository;

    @Inject
    private AccountRepository accountRepository;

    @Inject
    private UnlockComicService unlockService;


    /**
     * POST  account/unlock_comic : Unlock a comic by a player.
     *
     * @param UnlockComicDTO the unlock to processed.
     * @return the ResponseEntity with status 200 (Created) and with body the AccountInfoDTO information,
     * or with status 400 (Bad Request) if the username o comic are not registred,
     * or status 500 (Internal Server Error) if the unlock couldn't be done.
     *
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "Unlock a Comic",
            nickname = "unlock_comic",
            notes = "Unlock a Comic by a player")
    @RequestMapping(value = "/api/unlock_comic", method = RequestMethod.POST)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = ComicDTO.class),
        @ApiResponse(code = 400, message = "Invalid params"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Internal Server Error")})
    @Timed
    public ResponseEntity<?> unlockComic(
            @ApiParam(value="the unlock info to processed.", required=true) @Valid @RequestBody UnlockComicDTO unlockDTO,
            @ApiParam(value="enable debug.", required=false) @RequestParam(value="debug", required=false) Optional<Boolean> isDebugEnabled,
            HttpServletResponse response) {
        log.debug("REST request to unlock a comic");

        Comic comic = null;
        Optional<Comic> opComic = comicRepository.findFirstByCode(unlockDTO.getComicCode());
        log.debug("{}", opComic);
        if (opComic.isPresent()) {
            comic = opComic.get();
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(comic, HttpStatus.OK);
    }


    /**
     * POST  account/unlock_random : Unlock a random
     *
     * @param UnlockComicDTO the unlock to processed.
     * @return the ResponseEntity with status 200 (Created) and with body the AccountInfoDTO information,
     * or with status 400 (Bad Request) if the username o comic are not registred,
     * or status 500 (Internal Server Error) if the unlock couldn't be done.
     *
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "Unlock a Random Comic",
            nickname = "unlock_random",
            notes = "Unlock a Random Comic by a player")
    @RequestMapping(value = "/api/unlock_random/{username}", method = RequestMethod.POST)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = ComicDTO.class),
        @ApiResponse(code = 400, message = "Invalid params"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Internal Server Error")})
    @Timed
    public ResponseEntity<?> unlockRandom(
            @ApiParam(value="username", required=true) @PathVariable String username,
            @ApiParam(value="enable debug.", required=false) @RequestParam(value="debug", required=false) Optional<Boolean> isDebugEnabled,
            HttpServletResponse response) {
        log.debug("REST request to unlock a random comic for {} Account", username);


        Optional<Account> oAccount = accountRepository.findOneByUsername(username);
        if (!oAccount.isPresent()) {

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Optional<DigitalCopy> oCopy = unlockService.unlockARandomComic(oAccount.get());
        if(!oCopy.isPresent()) {
                HttpHeaders headers = HeaderUtil.createAlert(
                        "error.nothavevoucher", "request=unlock_random;account="+username);
                ErrorDTO errorDTO = new ErrorDTO(
                        ErrorConstants.ERR_VALIDATION, ErrorConstants.ERR_VALIDATION);
                errorDTO.add("Unlock", "voucher", "notHaveVoucherToUse");
                return new ResponseEntity<>(errorDTO, headers, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(oCopy.get(), HttpStatus.OK);
    }

}
