package qb9.web.rest.errors;

import java.io.Serializable;

public class FieldErrorDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String objectName;

    private final String field;

    private final String message;

    private final String code;


    public FieldErrorDTO(String dto, String field, String code, String message) {
        this.objectName = dto;
        this.field = field;
        this.code = code;
        this.message = message;
    }

    public FieldErrorDTO(String dto, String field, String code) {
        this(dto, field, code, code);
    }

    public String getObjectName() {
        return objectName;
    }

    public String getField() {
        return field;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "FieldErrorDTO{" +
            "objectName=" + objectName +
            ", field='" + field + "'" +
            ", code='" + code + "'" +
            ", message='" + message + "'" +
            '}';
    }


}
