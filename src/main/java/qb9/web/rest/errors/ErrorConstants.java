package qb9.web.rest.errors;

public final class ErrorConstants {

    public static final String ERR_CONCURRENCY_FAILURE = "error.concurrencyFailure";
    public static final String ERR_ACCESS_DENIED = "error.accessDenied";
    public static final String ERR_VALIDATION = "error.validation";
    public static final String ERR_MESSAGE_NOT_READABLE = "error.messageNotReadable";
    public static final String ERR_BAD_REQUEST = "error.bad_request";
    public static final String ERR_METHOD_NOT_SUPPORTED = "error.methodNotSupported";
    public static final String ERR_INTERNAL_SERVER_ERROR = "error.internalServerError";

    public static final String ERR_DESC_BAD_REQUEST = "Bad Request";
    public static final String ERR_DESC_VALIDATION = "Incorrect params request";

    private ErrorConstants() {
    }

}
