package qb9.web.rest;

import qb9.domain.enumeration.VoucherStatus;
import qb9.repository.VoucherRepository;
import qb9.repository.AccountRepository;
import qb9.domain.Voucher;
import qb9.domain.Account;
import qb9.service.AccountInfoService;
import qb9.service.FreeVoucherService;
import qb9.web.rest.dto.BuyVoucherDTO;
import qb9.web.rest.dto.AccountInfoDTO;
import qb9.web.rest.dto.AccountDTO;
import qb9.web.rest.mapper.AccountMapper;
import qb9.service.AccountService;

import com.codahale.metrics.annotation.Timed;

import java.time.ZonedDateTime;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import io.swagger.annotations.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Value;



@Component
@RestController
@Transactional
public class AccountInformationController {

    private final Logger log = LoggerFactory.getLogger(AccountInformationController.class);


    @Value("${qb9.comic.voucher_price}")
    private Integer voucherPrice;

    @Value("${qb9.comic.free_cooldown_time_in_hours}")
    private Integer coolDownTimeInHours;

    @Inject
    private AccountInfoService accountInfoService;

    @Inject
    private VoucherRepository voucherRepository;

    @Inject
    private AccountRepository accountRepository;

    @Inject
    private AccountService accountService;

    @Inject
    private FreeVoucherService freeVoucherService;


    @Inject
    private AccountMapper accountMapper;

    /**
     * POST  account/{username}/buy_voucher : Buy a comic voucher by a player.
     *
     * @param rewardDTO the reward info to processed.
     * @return the ResponseEntity with status 200 (Created) and with body the AccountInfoDTO information,
     * or with status 400 (Bad Request) if the player or goal are not registred,
     * or status 500 (Internal Server Error) if the reward couldn't be done.
     *
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "get account info",
            nickname = "account_info",
            notes = "Get Account Info")
        @RequestMapping(value = "/api/account_info/{username}", method = RequestMethod.GET)
        @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = AccountInfoDTO.class),
            @ApiResponse(code = 400, message = "Invalid params"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
        @Timed
        public ResponseEntity<?> accountInfo(
                @ApiParam(value="username", required=true) @PathVariable String username,
                @ApiParam(value="enable debug.", required=false) @RequestParam(value="debug", required=false) Optional<Boolean> isDebugEnabled,
                HttpServletResponse response) {
            log.debug("REST request to get a account info for {} Account", username);

            Account account = null;
            Optional<Account> oAccount = accountRepository.findOneByUsername(username);
            if (!oAccount.isPresent()) {
                log.debug("@Z Account no exists, process to create {}", username);
                account = accountService.save(accountService.create(username));
                log.debug("@Z Account for {} created", username);
                freeVoucherService.giveFreeVoucher(account);
                log.debug("@Z first voucher free created for new {} Account", username);
            } else {
                account = oAccount.get();
            }
            AccountInfoDTO accountInfoDTO = accountInfoService.resume(account);

        return new ResponseEntity<>(accountInfoDTO, HttpStatus.OK);
    }
}
