package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.domain.Account;
import qb9.service.AccountService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.web.rest.dto.AccountDTO;
import qb9.web.rest.mapper.AccountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    @Inject
    private AccountService accountService;

    @Inject
    private AccountMapper accountMapper;

    /**
     * POST  /accounts : Create a new account.
     *
     * @param accountDTO the accountDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new accountDTO, or with status 400 (Bad Request) if the account has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/accounts",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AccountDTO> createAccount(@RequestBody AccountDTO accountDTO) throws URISyntaxException {
        log.debug("REST request to save Account : {}", accountDTO);
        if (accountDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("account", "idexists", "A new account cannot already have an ID")).body(null);
        }
        AccountDTO result = accountService.save(accountDTO);
        return ResponseEntity.created(new URI("/api/accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("account", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /accounts : Updates an existing account.
     *
     * @param accountDTO the accountDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated accountDTO,
     * or with status 400 (Bad Request) if the accountDTO is not valid,
     * or with status 500 (Internal Server Error) if the accountDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/accounts",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AccountDTO> updateAccount(@RequestBody AccountDTO accountDTO) throws URISyntaxException {
        log.debug("REST request to update Account : {}", accountDTO);
        if (accountDTO.getId() == null) {
            return createAccount(accountDTO);
        }
        AccountDTO result = accountService.save(accountDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("account", accountDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /accounts : get all the accounts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of accounts in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/accounts",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<AccountDTO>> getAllAccounts(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Accounts");
        Page<Account> page = accountService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/accounts");
        return new ResponseEntity<>(accountMapper.accountsToAccountDTOs(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /accounts/:id : get the "id" account.
     *
     * @param id the id of the accountDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the accountDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/accounts/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<AccountDTO> getAccount(@PathVariable Long id) {
        log.debug("REST request to get Account : {}", id);
        AccountDTO accountDTO = accountService.findOne(id);
        return Optional.ofNullable(accountDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /accounts/:id : delete the "id" account.
     *
     * @param id the id of the accountDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/accounts/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteAccount(@PathVariable Long id) {
        log.debug("REST request to delete Account : {}", id);
        accountService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("account", id.toString())).build();
    }

}
