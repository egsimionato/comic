package qb9.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import qb9.domain.enumeration.ComicStatus;

/**
 * A Comic.
 */
@Entity
@Table(name = "tbl_comic")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Comic implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uid")
    private String uid;

    @Column(name = "code")
    private String code;

    @Column(name = "summary")
    private String summary;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "title")
    private String title;

    @Column(name = "safe_title")
    private String safeTitle;

    @Column(name = "number")
    private Integer number;

    @Column(name = "image_src")
    private String imageSrc;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ComicStatus status;

    public Long getId() {
        return id;
    }

    public Comic setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public Comic setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getCode() {
        return code;
    }

    public Comic setCode(String code) {
        this.code = code;
        return this;
    }

    public String getSummary() {
        return summary;
    }

    public Comic setSummary(String summary) {
        this.summary = summary;
        return this;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public Comic setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Comic setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Comic setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getSafeTitle() {
        return safeTitle;
    }

    public Comic setSafeTitle(String safeTitle) {
        this.safeTitle = safeTitle;
        return this;
    }

    public Integer getNumber() {
        return number;
    }

    public Comic setNumber(Integer number) {
        this.number = number;
        return this;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public Comic setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
        return this;
    }

    public ComicStatus getStatus() {
        return status;
    }

    public Comic setStatus(ComicStatus status) {
        this.status = status;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Comic comic = (Comic) o;
        if(comic.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, comic.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Comic{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", code='" + code + "'" +
            ", summary='" + summary + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", title='" + title + "'" +
            ", safeTitle='" + safeTitle + "'" +
            ", number='" + number + "'" +
            ", imageSrc='" + imageSrc + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
