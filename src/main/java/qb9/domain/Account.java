package qb9.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import qb9.domain.enumeration.AccountStatus;

/**
 * A Account.
 */
@Entity
@Table(name = "tbl_account")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uid")
    private String uid;

    @Column(name = "email")
    private String email;

    @Column(name = "username")
    private String username;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "last_freed_at")
    private ZonedDateTime lastFreedAt;

    @Column(name = "last_loged_at")
    private ZonedDateTime lastLogedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private AccountStatus status;

    public Long getId() {
        return id;
    }

    public Account setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public Account setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Account setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public Account setUsername(String username) {
        this.username = username;
        return this;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public Account setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Account setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public ZonedDateTime getLastFreedAt() {
        return lastFreedAt;
    }

    public Account setLastFreedAt(ZonedDateTime lastFreedAt) {
        this.lastFreedAt = lastFreedAt;
        return this;
    }

    public ZonedDateTime getLastLogedAt() {
        return lastLogedAt;
    }

    public Account setLastLogedAt(ZonedDateTime lastLogedAt) {
        this.lastLogedAt = lastLogedAt;
        return this;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public Account setStatus(AccountStatus status) {
        this.status = status;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        if(account.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, account.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Account{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", email='" + email + "'" +
            ", username='" + username + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", lastFreedAt='" + lastFreedAt + "'" +
            ", lastLogedAt='" + lastLogedAt + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
