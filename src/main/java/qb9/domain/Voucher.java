package qb9.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import qb9.domain.enumeration.VoucherType;

import qb9.domain.enumeration.VoucherStatus;

/**
 * A Voucher.
 */
@Entity
@Table(name = "tbl_voucher")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Voucher implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uid")
    private String uid;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private VoucherType type;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private VoucherStatus status;

    @ManyToOne
    private Account account;

    @OneToOne(mappedBy = "voucher")
    @JsonIgnore
    private DigitalCopy digitalCopy;

    public Long getId() {
        return id;
    }

    public Voucher setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public Voucher setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public VoucherType getType() {
        return type;
    }

    public Voucher setType(VoucherType type) {
        this.type = type;
        return this;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public Voucher setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public Voucher setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public VoucherStatus getStatus() {
        return status;
    }

    public Voucher setStatus(VoucherStatus status) {
        this.status = status;
        return this;
    }

    public Account getAccount() {
        return account;
    }

    public Voucher setAccount(Account account) {
        this.account = account;
        return this;
    }

    public DigitalCopy getDigitalCopy() {
        return digitalCopy;
    }

    public Voucher setDigitalCopy(DigitalCopy digitalCopy) {
        this.digitalCopy = digitalCopy;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Voucher voucher = (Voucher) o;
        if(voucher.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, voucher.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Voucher{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", type='" + type + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
