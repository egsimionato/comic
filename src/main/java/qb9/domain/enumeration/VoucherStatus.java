package qb9.domain.enumeration;

/**
 * The VoucherStatus enumeration.
 */
public enum VoucherStatus {
    NEW,REDEEMED,USED
}
