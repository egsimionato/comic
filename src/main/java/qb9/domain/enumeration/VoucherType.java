package qb9.domain.enumeration;

/**
 * The VoucherType enumeration.
 */
public enum VoucherType {
    BUY,FREE,REWARD
}
