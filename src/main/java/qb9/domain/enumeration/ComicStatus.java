package qb9.domain.enumeration;

/**
 * The ComicStatus enumeration.
 */
public enum ComicStatus {
    DRAFT,NEW,ACTIVE,CANCEL
}
