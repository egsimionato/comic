package qb9.domain.enumeration;

/**
 * The CopyStatus enumeration.
 */
public enum CopyStatus {
    ACTIVE,DISABLED
}
