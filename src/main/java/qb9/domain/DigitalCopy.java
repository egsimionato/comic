package qb9.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import qb9.domain.enumeration.CopyStatus;

/**
 * A DigitalCopy.
 */
@Entity
@Table(name = "tbl_digital_copy")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class DigitalCopy implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uid")
    private String uid;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private CopyStatus status;

    @Column(name = "username")
    private String username;

    @ManyToOne
    private Comic comic;

    @ManyToOne
    private Account account;

    @OneToOne
    @JoinColumn(unique = true)
    private Voucher voucher;

    public Long getId() {
        return id;
    }

    public DigitalCopy setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public DigitalCopy setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public DigitalCopy setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public DigitalCopy setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public CopyStatus getStatus() {
        return status;
    }

    public DigitalCopy setStatus(CopyStatus status) {
        this.status = status;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public DigitalCopy setUsername(String username) {
        this.username = username;
        return this;
    }

    public Comic getComic() {
        return comic;
    }

    public DigitalCopy setComic(Comic comic) {
        this.comic = comic;
        return this;
    }

    public Account getAccount() {
        return account;
    }

    public DigitalCopy setAccount(Account account) {
        this.account = account;
        return this;
    }

    public Voucher getVoucher() {
        return voucher;
    }

    public DigitalCopy setVoucher(Voucher voucher) {
        this.voucher = voucher;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DigitalCopy digitalCopy = (DigitalCopy) o;
        if(digitalCopy.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, digitalCopy.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "DigitalCopy{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", status='" + status + "'" +
            ", username='" + username + "'" +
            '}';
    }
}
