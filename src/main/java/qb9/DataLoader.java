package qb9;

import qb9.config.Constants;
import qb9.domain.Account;
import qb9.domain.Comic;
import qb9.domain.DigitalCopy;
import qb9.domain.Voucher;
import qb9.domain.enumeration.AccountStatus;
import qb9.domain.enumeration.ComicStatus;
import qb9.domain.enumeration.CopyStatus;
import qb9.domain.enumeration.VoucherStatus;
import qb9.domain.enumeration.VoucherType;
import qb9.repository.AccountRepository;
import qb9.repository.ComicRepository;
import qb9.repository.DigitalCopyRepository;
import qb9.repository.VoucherRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PostConstruct;
import javax.inject.Inject;



@Configuration
@Profile("dev")
public class DataLoader {


    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_CODE = "AAAAA";
    private static final String DEFAULT_EMAIL = "AAAAA";
    private static final String DEFAULT_SUMMARY = "AAAAA";
    private static final String DEFAULT_UID = "AAAAA";
    private static final String DEFAULT_USERNAME = "AAAAA";
    private static final String UPDATED_CODE = "BBBBB";
    private static final String UPDATED_EMAIL = "BBBBB";
    private static final String UPDATED_SUMMARY = "BBBBB";
    private static final String UPDATED_UID = "BBBBB";
    private static final String UPDATED_USERNAME = "BBBBB";


    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    //private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now();
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);


    private static final String DEFAULT_TITLE = "AAAAA";
    private static final String UPDATED_TITLE = "BBBBB";
    private static final String DEFAULT_SAFE_TITLE = "AAAAA";
    private static final String UPDATED_SAFE_TITLE = "BBBBB";

    private static final Integer DEFAULT_NUMBER = 1;
    private static final Integer UPDATED_NUMBER = 2;
    private static final String DEFAULT_IMAGE_SRC = "AAAAA";
    private static final String UPDATED_IMAGE_SRC = "BBBBB";


    private final Logger log = LoggerFactory.getLogger(DataLoader.class);

    @Inject
    private AccountRepository accountRepository;

    @Inject
    private VoucherRepository voucherRepository;

    @Inject
    private ComicRepository comicRepository;

    @Inject
    private DigitalCopyRepository copyRepository;


    @Inject
    private Environment env;


    @PostConstruct
    public void loadData() {
        log.debug("Loading sample data v3");
        log.debug("now() => {}", ZonedDateTime.now());
        copyRepository.deleteAll();
        voucherRepository.deleteAll();
        comicRepository.deleteAll();
        accountRepository.deleteAll();

        Account matildaAccount = fakeAccount("matilda");
        accountRepository.save(matildaAccount);
        Account virginiaAccount = fakeAccount("virginia");
        accountRepository.save(virginiaAccount);


        Voucher matildaFirstVoucher = fakeVoucher(matildaAccount);
        voucherRepository.save(matildaFirstVoucher);

        Voucher virginiaFirstVoucher = fakeVoucher(virginiaAccount);
        voucherRepository.save(virginiaFirstVoucher);


        Comic comicOne = fakeComic("one", 1);
        comicRepository.save(comicOne);

        DigitalCopy copyOneMatute = fakeCopy(matildaAccount, comicOne, matildaFirstVoucher);
        copyRepository.save(copyOneMatute);
        voucherRepository.save(matildaFirstVoucher);
        DigitalCopy copyOneVirginia = fakeCopy(virginiaAccount, comicOne, virginiaFirstVoucher);
        copyRepository.save(copyOneVirginia);
        voucherRepository.save(virginiaFirstVoucher);

        Comic comicTwo = fakeComic("two", 2);
        comicRepository.save(comicTwo);
        Comic comicThree = fakeComic("tree", 3);
        comicRepository.save(comicThree);
        Comic comicFour = fakeComic("four", 4);
        comicRepository.save(comicFour);



        log.debug("Sample data loaded");
    }

    private Comic fakeComic() {
        Comic comic = new Comic();
        comic.setUid("comic.comic."+UUID.randomUUID());
        comic.setCode(DEFAULT_CODE);
        comic.setSummary(DEFAULT_SUMMARY);
        comic.setCreatedAt(DEFAULT_CREATED_AT);
        comic.setUpdatedAt(DEFAULT_UPDATED_AT);
        comic.setTitle(DEFAULT_TITLE);
        comic.setSafeTitle(DEFAULT_SAFE_TITLE);
        comic.setNumber(DEFAULT_NUMBER);
        comic.setImageSrc(DEFAULT_IMAGE_SRC);
        comic.setStatus(ComicStatus.NEW);
        return comic;
    }


    private Comic fakeComic(String suffix, Integer number) {
        Comic comic = fakeComic();
        comic.setCode("comic.gaturro" + suffix)
            .setNumber(number)
            .setTitle("Gaturro " + suffix)
            .setSafeTitle("gaturro-" + suffix)
            .setSummary("Gaturro en una Aventura " + suffix + ".")
            .setStatus(ComicStatus.ACTIVE)
            .setImageSrc("http://localhost:9090/images/comics/gaturro." + suffix + ".jpg");
        return comic;
    }

    private Account fakeAccount() {
        Account account = new Account();
        account.setUid("comic.account."+UUID.randomUUID())
            .setEmail(DEFAULT_EMAIL)
            .setUsername(DEFAULT_USERNAME)
            .setCreatedAt(DEFAULT_CREATED_AT)
            .setUpdatedAt(DEFAULT_UPDATED_AT)
            .setLastFreedAt(ZonedDateTime.now())
            .setLastLogedAt(UPDATED_UPDATED_AT)
            .setStatus(AccountStatus.NEW);
        return account;
    }

    private Account fakeAccount(String username) {
        return fakeAccount()
            .setUsername(username)
            .setEmail(username+"@mail.com");
    }

    private DigitalCopy fakeCopy(Account account, Comic comic, Voucher voucher) {
        DigitalCopy copy = new DigitalCopy();
        copy.setUid("comic.copy."+UUID.randomUUID())
            .setAccount(account)
            .setComic(comic)
            .setVoucher(voucher)
            .setCreatedAt(UPDATED_UPDATED_AT)
            .setUpdatedAt(UPDATED_UPDATED_AT)
            .setUsername(account.getUsername())
            .setStatus(CopyStatus.ACTIVE);
        voucher.setStatus(VoucherStatus.USED);
        return copy;
    }


    private Voucher fakeVoucher() {
        Voucher voucher = new Voucher();
        voucher.setUid("comic.voucher."+UUID.randomUUID());
        voucher.setType(VoucherType.FREE);
        voucher.setCreatedAt(UPDATED_UPDATED_AT);
        voucher.setUpdatedAt(UPDATED_UPDATED_AT);
        voucher.setStatus(VoucherStatus.NEW);
        return voucher;
    }

    private Voucher fakeVoucher(Account account) {
        Voucher voucher = fakeVoucher();
        voucher.setAccount(account);
        return voucher;
    }


}
