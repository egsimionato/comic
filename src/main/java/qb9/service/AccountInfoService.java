package qb9.service;

import qb9.domain.Account;
import qb9.web.rest.dto.AccountInfoDTO;

public interface AccountInfoService {

    AccountInfoDTO resume(Account account);

}
