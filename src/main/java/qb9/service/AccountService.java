package qb9.service;

import qb9.domain.Account;
import qb9.domain.enumeration.AccountStatus;
import qb9.web.rest.dto.AccountDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
/**
 * Service Interface for managing Account.
 */
public interface AccountService {

    /**
     * Save a account.
     *
     * @param accountDTO the entity to save
     * @return the persisted entity
     */
    AccountDTO save(AccountDTO accountDTO);

    /**
     * Save a account.
     *
     * @param account the entity to save
     * @return the persisted entity
     */
    Account save(Account account);


    /**
     *  Get all the accounts.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Account> findAll(Pageable pageable);

    /**
     *  Get the "id" account.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    AccountDTO findOne(Long id);

    /**
     *  Delete the "id" account.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     *  Get the "username" account.
     *
     *  @param username the username of the entity
     *  @return the entity
     */
    AccountDTO findByUsername(String username);


    Account create(String username);

}
