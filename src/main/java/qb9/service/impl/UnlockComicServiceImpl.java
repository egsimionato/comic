package qb9.service.impl;

import qb9.domain.DigitalCopy;
import qb9.domain.Account;
import qb9.domain.Voucher;
import qb9.domain.Comic;
import qb9.domain.enumeration.VoucherStatus;
import qb9.repository.VoucherRepository;
import qb9.repository.ComicRepository;
import qb9.repository.DigitalCopyRepository;
import qb9.service.DigitalCopyService;

import qb9.service.UnlockComicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import javax.inject.Inject;

@Service
@Transactional
public class UnlockComicServiceImpl implements UnlockComicService {

    private final Logger log = LoggerFactory.getLogger(UnlockComicServiceImpl.class);

    @Inject
    ComicRepository comicRepository;

    @Inject
    DigitalCopyService copyService;

    @Inject
    VoucherRepository voucherRepository;

    @Inject
    DigitalCopyRepository copyRepository;


    public Optional<DigitalCopy> unlockARandomComic(Account account) {
        log.debug("Request to perform a random unlock for {} Account", account.getUsername());

        // has Voucher Free //
        Optional<Voucher> oVoucher = voucherRepository.findFirstByAccountAndStatus(account, VoucherStatus.NEW);
        if (!oVoucher.isPresent()) {
            log.debug("@Z {} Account does not have a Voucher to use", account.getUsername());
            return Optional.ofNullable(null);
        }
        Voucher voucher = oVoucher.get();
        voucher.setStatus(VoucherStatus.USED);
        voucherRepository.save(voucher);

        Long lastComicId = -1l;
        Optional<DigitalCopy> oTopCopy = copyRepository.findFirstByAccountOrderByComicIdDesc(account);
        DigitalCopy topCopy = null;
        if (oTopCopy.isPresent()) {
            topCopy = oTopCopy.get();
            log.debug("@Z {} Account have a TOP copy {} for Comic {}", account.getUsername(), topCopy.getId(), topCopy.getComic().getId());
            lastComicId = topCopy.getComic().getId();
        }
        log.debug("@Z {} Account have a LAST COMIC ID {}", account.getUsername(), lastComicId);

        Optional<Comic> oNextComic = comicRepository.findFirstByIdGreaterThan(lastComicId);
        if (!oNextComic.isPresent()) {
            log.debug("@Z {} Account already have all Comic", account.getUsername());
            return Optional.ofNullable(null);
        }
        Comic nextComic = oNextComic.get();

        DigitalCopy newCopy = copyService.create();
        newCopy.setAccount(account);
        newCopy.setComic(nextComic);
        newCopy.setVoucher(voucher);
        newCopy = copyService.save(newCopy);

        return Optional.of(newCopy);
    }

}
