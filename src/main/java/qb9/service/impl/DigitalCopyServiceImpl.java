package qb9.service.impl;

import qb9.domain.DigitalCopy;
import qb9.domain.enumeration.CopyStatus;
import qb9.repository.DigitalCopyRepository;
import qb9.service.DigitalCopyService;
import qb9.web.rest.dto.DigitalCopyDTO;
import qb9.web.rest.mapper.DigitalCopyMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

import java.time.ZonedDateTime;
import java.util.UUID;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing DigitalCopy.
 */
@Service
@Transactional
public class DigitalCopyServiceImpl implements DigitalCopyService{

    private final Logger log = LoggerFactory.getLogger(DigitalCopyServiceImpl.class);

    @Inject
    private DigitalCopyRepository digitalCopyRepository;

    @Inject
    private DigitalCopyMapper digitalCopyMapper;

    /**
     * Save a digitalCopy.
     *
     * @param digitalCopyDTO the entity to save
     * @return the persisted entity
     */
    public DigitalCopyDTO save(DigitalCopyDTO digitalCopyDTO) {
        DigitalCopy digitalCopy = digitalCopyMapper.digitalCopyDTOToDigitalCopy(digitalCopyDTO);
        digitalCopy = save(digitalCopy);
        DigitalCopyDTO result = digitalCopyMapper.digitalCopyToDigitalCopyDTO(digitalCopy);
        return result;
    }

    /**
     * Save a digitalCopy.
     *
     * @param digitalCopy the entity to save
     * @return the persisted entity
     */
    public DigitalCopy save(DigitalCopy digitalCopy) {
        log.debug("Request to save DigitalCopy : {}", digitalCopy);
        return digitalCopyRepository.save(digitalCopy);
    }



    /**
     *  Get all the digitalCopies.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DigitalCopy> findAll(Pageable pageable) {
        log.debug("Request to get all DigitalCopies");
        Page<DigitalCopy> result = digitalCopyRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one digitalCopy by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public DigitalCopyDTO findOne(Long id) {
        log.debug("Request to get DigitalCopy : {}", id);
        DigitalCopy digitalCopy = digitalCopyRepository.findOne(id);
        DigitalCopyDTO digitalCopyDTO = digitalCopyMapper.digitalCopyToDigitalCopyDTO(digitalCopy);
        return digitalCopyDTO;
    }

    /**
     *  Delete the  digitalCopy by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DigitalCopy : {}", id);
        digitalCopyRepository.delete(id);
    }



    /**
     *  Get the digitalCopies by username.
     *
     *  @param username the username of the entities
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<DigitalCopy> findByUsername(String username, Pageable pageable) {
        log.debug("Request to get DigitalCopies by username");
        Page<DigitalCopy> result = digitalCopyRepository.findByAccountUsername(username, pageable);
        return result;
    }

    public DigitalCopy create() {
       ZonedDateTime now = ZonedDateTime.now();
       DigitalCopy copy = new DigitalCopy();
        copy.setUid("comic.copy."+UUID.randomUUID())
            .setCreatedAt(now)
            .setUpdatedAt(now)
            .setStatus(CopyStatus.ACTIVE);
        return copy;
    }


}
