package qb9.service.impl;

import qb9.service.ComicService;
import qb9.domain.Comic;
import qb9.repository.ComicRepository;
import qb9.web.rest.dto.ComicDTO;
import qb9.web.rest.mapper.ComicMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Comic.
 */
@Service
@Transactional
public class ComicServiceImpl implements ComicService{

    private final Logger log = LoggerFactory.getLogger(ComicServiceImpl.class);

    @Inject
    private ComicRepository comicRepository;

    @Inject
    private ComicMapper comicMapper;

    /**
     * Save a comic.
     *
     * @param comicDTO the entity to save
     * @return the persisted entity
     */
    public ComicDTO save(ComicDTO comicDTO) {
        Comic comic = comicMapper.comicDTOToComic(comicDTO);
        comic = save(comic);
        ComicDTO result = comicMapper.comicToComicDTO(comic);
        return result;
    }

    public Comic save(Comic comic) {
        log.debug("Request to save Comic : {}", comic);
         return comicRepository.save(comic);
    }

    /**
     *  Get all the comics.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Comic> findAll(Pageable pageable) {
        log.debug("Request to get all Comics");
        Page<Comic> result = comicRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one comic by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ComicDTO findOne(Long id) {
        log.debug("Request to get Comic : {}", id);
        Comic comic = comicRepository.findOne(id);
        ComicDTO comicDTO = comicMapper.comicToComicDTO(comic);
        return comicDTO;
    }

    /**
     *  Delete the  comic by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Comic : {}", id);
        comicRepository.delete(id);
    }
}
