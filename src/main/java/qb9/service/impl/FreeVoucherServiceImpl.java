package qb9.service.impl;

import qb9.service.FreeVoucherService;
import qb9.service.AccountInfoService;
import qb9.service.VoucherService;
import qb9.service.AccountService;
import qb9.web.rest.dto.AccountInfoDTO;
import qb9.domain.Account;
import qb9.domain.Voucher;
import qb9.service.VoucherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import java.time.ZonedDateTime;

@Service
@Transactional
public class FreeVoucherServiceImpl implements FreeVoucherService {

    private final Logger log = LoggerFactory.getLogger(FreeVoucherServiceImpl.class);

    @Inject
    private AccountInfoService accountInfoService;

    @Inject
    private AccountService accountService;

    @Inject
    private VoucherService voucherService;

    public AccountInfoDTO perform(Account account) {
        log.debug("Request to perform a free voucher for {} Account", account.getUsername());
        AccountInfoDTO accResumeDTO = accountInfoService.resume(account);
        log.trace("@Z account resume for {} : {}", account.getUsername(), accResumeDTO);
        if(accResumeDTO.getFreeVoucherElapsedTimeInSeconds()<0) {
            Voucher voucher = giveFreeVoucher(account);
            log.debug("Free voucher {} awarded for {} Account", voucher.getUid(), account.getUsername());
        }
        accResumeDTO = accountInfoService.resume(account);
        return accResumeDTO;
    }

    public Voucher giveFreeVoucher(Account account) {
            Voucher voucher = voucherService.createFreeVoucher();
            voucher.setAccount(account);
            voucherService.save(voucher);
            account.setLastFreedAt(ZonedDateTime.now());
            accountService.save(account);
            return voucher;
    }

}
