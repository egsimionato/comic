package qb9.service.impl;

import qb9.service.VoucherService;
import qb9.domain.Voucher;
import qb9.domain.enumeration.VoucherStatus;
import qb9.domain.enumeration.VoucherType;
import qb9.repository.VoucherRepository;
import qb9.web.rest.dto.VoucherDTO;
import qb9.web.rest.mapper.VoucherMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing Voucher.
 */
@Service
@Transactional
public class VoucherServiceImpl implements VoucherService{

    private final Logger log = LoggerFactory.getLogger(VoucherServiceImpl.class);

    @Inject
    private VoucherRepository voucherRepository;

    @Inject
    private VoucherMapper voucherMapper;

    /**
     * Save a voucher.
     *
     * @param voucherDTO the entity to save
     * @return the persisted entity
     */
    public VoucherDTO save(VoucherDTO voucherDTO) {
        Voucher voucher = voucherMapper.voucherDTOToVoucher(voucherDTO);
        voucher = save(voucher);
        VoucherDTO result = voucherMapper.voucherToVoucherDTO(voucher);
        return result;
    }


    /**
     * Save a voucher.
     *
     * @param voucher the entity to save
     * @return the persisted entity
     */
    public Voucher save(Voucher voucher) {
        log.debug("Request to save Voucher : {}", voucher.getUid());
        voucher = voucherRepository.save(voucher);
        return voucher;
    }

    /**
     *  Get all the vouchers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Voucher> findAll(Pageable pageable) {
        log.debug("Request to get all Vouchers");
        Page<Voucher> result = voucherRepository.findAll(pageable);
        return result;
    }


    /**
     *  get all the vouchers where DigitalCopy is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<VoucherDTO> findAllWhereDigitalCopyIsNull() {
        log.debug("Request to get all vouchers where DigitalCopy is null");
        return StreamSupport
            .stream(voucherRepository.findAll().spliterator(), false)
            .filter(voucher -> voucher.getDigitalCopy() == null)
            .map(voucherMapper::voucherToVoucherDTO)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one voucher by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public VoucherDTO findOne(Long id) {
        log.debug("Request to get Voucher : {}", id);
        Voucher voucher = voucherRepository.findOne(id);
        VoucherDTO voucherDTO = voucherMapper.voucherToVoucherDTO(voucher);
        return voucherDTO;
    }

    /**
     *  Delete the  voucher by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Voucher : {}", id);
        voucherRepository.delete(id);
    }


    public Voucher createFreeVoucher() {
        ZonedDateTime now = ZonedDateTime.now();
        Voucher voucher = new Voucher();
        voucher.setUid("comic.voucher."+UUID.randomUUID());
        voucher.setType(VoucherType.FREE);
        voucher.setCreatedAt(now);
        voucher.setUpdatedAt(now);
        voucher.setStatus(VoucherStatus.NEW);
        return voucher;
    }

    /**
     *  Get the vouchers by username.
     *
     *  @param username the username of the entities
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Voucher> findByUsername(String username, Pageable pageable) {
        log.debug("Request to get Vouchers by username");
        Page<Voucher> result = voucherRepository.findByAccountUsername(username, pageable);
        return result;
    }

}
