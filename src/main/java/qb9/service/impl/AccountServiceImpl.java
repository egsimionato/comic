package qb9.service.impl;

import qb9.domain.Account;
import qb9.domain.enumeration.AccountStatus;
import qb9.repository.AccountRepository;
import qb9.service.AccountService;
import qb9.web.rest.dto.AccountDTO;
import qb9.web.rest.mapper.AccountMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing Account.
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService{

    private final Logger log = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Inject
    private AccountRepository accountRepository;

    @Inject
    private AccountMapper accountMapper;

    /**
     * Save a account.
     *
     * @param accountDTO the entity to save
     * @return the persisted entity
     */
    public AccountDTO save(AccountDTO accountDTO) {
        Account account = accountMapper.accountDTOToAccount(accountDTO);
        account = this.save(account);
        AccountDTO result = accountMapper.accountToAccountDTO(account);
        return result;
    }

    public Account save(Account account) {
        log.debug("Request to save {} Account", account.getUsername());
        account = accountRepository.save(account);
        return account;
    }


    /**
     *  Get all the accounts.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Account> findAll(Pageable pageable) {
        log.debug("Request to get all Accounts");
        Page<Account> result = accountRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one account by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AccountDTO findOne(Long id) {
        log.debug("Request to get Account : {}", id);
        Account account = accountRepository.findOne(id);
        AccountDTO accountDTO = accountMapper.accountToAccountDTO(account);
        return accountDTO;
    }

    /**
     *  Delete the  account by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Account : {}", id);
        accountRepository.delete(id);
    }

    /**
     *  Get one account by username.
     *
     *  @param username the username of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AccountDTO findByUsername(String username) {
        log.debug("Request to get Account : {}", username);
        Optional<Account> oAccount = accountRepository.findOneByUsername(username);
        AccountDTO accountDTO = accountMapper.accountToAccountDTO(oAccount.get());
        return accountDTO;
    }

    public Account create(String username) {
        Account account = new Account();
        ZonedDateTime now = ZonedDateTime.now(ZoneId.systemDefault());
        account.setUid("comic.account."+UUID.randomUUID())
            .setEmail(username+"@na.na")
            .setUsername(username)
            .setCreatedAt(now)
            .setUpdatedAt(now)
            .setLastFreedAt(now)
            .setLastLogedAt(null)
            .setStatus(AccountStatus.NEW);
        return account;
    }


}
