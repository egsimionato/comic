package qb9.service.impl;

import qb9.domain.Account;
import qb9.domain.Voucher;
import qb9.domain.enumeration.VoucherStatus;
import qb9.repository.VoucherRepository;
import qb9.service.AccountInfoService;
import qb9.web.rest.dto.AccountInfoDTO;

import javax.inject.Inject;

import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.time.ZonedDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Value;

@Service
@Transactional
public class AccountInfoServiceImpl implements AccountInfoService {

    private final Logger log = LoggerFactory.getLogger(AccountInfoServiceImpl.class);

    @Value("${qb9.comic.voucher_price}")
    private Integer voucherPrice;

    @Value("${qb9.comic.free_cooldown_time_in_hours}")
    private Integer coolDownTimeInHours;

    @Inject
    private VoucherRepository voucherRepository;

    public AccountInfoDTO resume(Account account) {
        log.debug("Request to resume {} Account", account.getUsername());

        Long newVouchersQty = new Long(voucherRepository.countByAccountAndStatus(
                    account, VoucherStatus.NEW));

        AccountInfoDTO accountInfoDTO = new AccountInfoDTO();
        accountInfoDTO.setUsername(account.getUsername())
            .setId(account.getId())
            .setUid(account.getUid())
            .setLastFreedAt(account.getLastFreedAt())
            .setComingFreedAt(calculateComingFreedAt(account.getLastFreedAt()))
            .setVouchersAvailableQty(newVouchersQty)
            .setVoucherPrice(voucherPrice);
        accountInfoDTO.setFreeVoucherElapsedTimeInSeconds(
                elapsedTimeInSeconds(accountInfoDTO.getComingFreedAt()));

        return accountInfoDTO;
    }

    private ZonedDateTime calculateComingFreedAt(ZonedDateTime lastFreedAt) {
        return lastFreedAt.plusHours(coolDownTimeInHours);
    }

    private long elapsedTimeInSeconds(ZonedDateTime coming) {
        return Duration.between(ZonedDateTime.now(), coming).getSeconds();
    }

}
