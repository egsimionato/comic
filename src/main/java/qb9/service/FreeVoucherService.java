package qb9.service;

import qb9.web.rest.dto.AccountInfoDTO;
import qb9.domain.Account;
import qb9.domain.Voucher;

public interface FreeVoucherService {

    public AccountInfoDTO perform(Account account);

    public Voucher giveFreeVoucher(Account account);

}
