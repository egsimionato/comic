package qb9.service;

import qb9.domain.Comic;
import qb9.web.rest.dto.ComicDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Comic.
 */
public interface ComicService {

    /**
     * Save a comic.
     *
     * @param comicDTO the entity to save
     * @return the persisted entity
     */
    ComicDTO save(ComicDTO comicDTO);

    /**
     * Save a comic.
     *
     * @param comic the entity to save
     * @return the persisted entity
     */
    Comic save(Comic comic);



    /**
     *  Get all the comics.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Comic> findAll(Pageable pageable);

    /**
     *  Get the "id" comic.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ComicDTO findOne(Long id);

    /**
     *  Delete the "id" comic.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
