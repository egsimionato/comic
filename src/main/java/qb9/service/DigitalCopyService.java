package qb9.service;

import qb9.domain.DigitalCopy;
import qb9.web.rest.dto.DigitalCopyDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing DigitalCopy.
 */
public interface DigitalCopyService {

    /**
     * Save a digitalCopy.
     *
     * @param digitalCopyDTO the entity to save
     * @return the persisted entity
     */
    DigitalCopyDTO save(DigitalCopyDTO digitalCopyDTO);

    /**
     * Save a digitalCopy.
     *
     * @param digitalCopy the entity to save
     * @return the persisted entity
     */
    DigitalCopy save(DigitalCopy digitalCopy);


    /**
     *  Get all the digitalCopies.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<DigitalCopy> findAll(Pageable pageable);

    /**
     *  Get the "id" digitalCopy.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    DigitalCopyDTO findOne(Long id);

    /**
     *  Delete the "id" digitalCopy.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Get the digitalCopies by username.
     *
     * @param username the username
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<DigitalCopy> findByUsername(String username, Pageable pageable);


    DigitalCopy create();

}
