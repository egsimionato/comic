package qb9.service;

import qb9.domain.Account;
import qb9.domain.DigitalCopy;

import java.util.Optional;

public interface UnlockComicService {

    Optional<DigitalCopy> unlockARandomComic(Account account);

}
