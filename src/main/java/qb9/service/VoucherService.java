package qb9.service;

import qb9.domain.Voucher;
import qb9.web.rest.dto.VoucherDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Voucher.
 */
public interface VoucherService {

    /**
     * Save a voucher.
     *
     * @param voucherDTO the entity to save
     * @return the persisted entity
     */
    VoucherDTO save(VoucherDTO voucherDTO);

    /**
     * Save a voucher.
     *
     * @param voucher the entity to save
     * @return the persisted entity
     */
    public Voucher save(Voucher voucher);


    /**
     *  Get all the vouchers.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Voucher> findAll(Pageable pageable);
    /**
     *  Get all the vouchers where DigitalCopy is null.
     *
     *  @return the list of entities
     */
    List<VoucherDTO> findAllWhereDigitalCopyIsNull();

    /**
     *  Get the "id" voucher.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    VoucherDTO findOne(Long id);

    /**
     *  Delete the "id" voucher.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);


    Voucher createFreeVoucher();


    /**
     * Get the voucher by username.
     *
     * @param username the username
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Voucher> findByUsername(String username, Pageable pageable);



}
