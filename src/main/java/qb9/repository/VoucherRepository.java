package qb9.repository;

import qb9.domain.Voucher;
import qb9.domain.enumeration.VoucherStatus;
import qb9.domain.Account;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Voucher entity.
 */
@SuppressWarnings("unused")
public interface VoucherRepository extends JpaRepository<Voucher,Long> {

    public long countByAccountAndStatus(Account account, VoucherStatus status);
    public long countByAccount(Account account);
    public long countByStatus(VoucherStatus status);
    Page<Voucher> findByAccountUsername(String username, Pageable pageable);
    Optional<Voucher> findOneByAccountAndStatus(Account account, VoucherStatus status);
    Optional<Voucher> findFirstByAccountAndStatus(Account account, VoucherStatus status);

}
