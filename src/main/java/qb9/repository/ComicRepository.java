package qb9.repository;

import qb9.domain.Comic;
import qb9.domain.enumeration.ComicStatus;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data JPA repository for the Comic entity.
 */
@SuppressWarnings("unused")
public interface ComicRepository extends JpaRepository<Comic,Long> {

    @Query("SELECT max(t.id) FROM #{#entityName} t")
    Integer getMaxId();

    Optional<Comic> findOneById(Long goalId);

    Optional<Comic> findOneByCode(String code);
    Optional<Comic> findFirstByCode(String code);

    List<Comic> findByStatus(ComicStatus status);
    Optional<Comic> findFirstByStatus(ComicStatus status);

    Optional<Comic> findFirstByIdGreaterThan(Long lastId);
}
