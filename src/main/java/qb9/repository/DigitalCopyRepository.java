package qb9.repository;

import qb9.domain.DigitalCopy;
import qb9.domain.Account;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the DigitalCopy entity.
 */
@SuppressWarnings("unused")
public interface DigitalCopyRepository extends JpaRepository<DigitalCopy,Long> {

    Page<DigitalCopy> findByUsername(String username, Pageable pageable);

    Page<DigitalCopy> findByAccountUsername(String username, Pageable pageable);

    Optional<DigitalCopy> findTopByAccount(Account account);

    Optional<DigitalCopy> findTopByAccountOrderByComicId(Account account);

    Optional<DigitalCopy> findFirstByAccountOrderByComicIdDesc(Account account);

}
