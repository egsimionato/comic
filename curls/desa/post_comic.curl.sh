#! /bin/bash
curl -X POST --header 'Content-Type: application/json' \
    --header 'Accept: application/json' \
    --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ3MTUzMzAwNH0.aDJt7ETyizI6paesQmcBcVXJi6a_VfQfh6uuBKY7024fjKHOFVshL8NsNvYwywPdoaYzo3sCnc0doUAh3k76RQ' \
    -d '{
    "uid": "gaturro1",
    "code": "comic.gaturro1",
    "description": "Comic de Gaturro Nro 1",
    "title": "El Mundo de Gaturro Octubre",
    "title": "mundo-gaturr-1",
    "number": "0001",
    "image_src": "http://images.ucomics.com/images/uclick/spgat/spgat040605.gif"
    }' 'http://127.0.0.1:8082/api/comics'
# echo "\ndone!\n";
