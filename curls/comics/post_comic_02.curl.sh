#! bin/bash
curl -X POST --header 'Content-Type: application/json' \
    --header 'Accept: application/json' \
    --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ3MTUzMzAwNH0.aDJt7ETyizI6paesQmcBcVXJi6a_VfQfh6uuBKY7024fjKHOFVshL8NsNvYwywPdoaYzo3sCnc0doUAh3k76RQ' \
    -d '{
    "uid": "gaturro2",
    "code": "comic.gaturro2",
    "description": "Comic de Gaturro Nro 2",
    "title": "El Mundo de Gaturro Noviembre",
    "safeTitle": "mundo-gaturro-2",
    "number": "0002",
    "imageSrc": "https://infomundogaturro.files.wordpress.com/2011/02/foto-blog-44.png"
    }' 'http://127.0.0.1:8070/api/comics'
echo "\ndone!\n";
