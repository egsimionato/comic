#! bin/bash
curl -X POST --header 'Content-Type: application/json' \
    --header 'Accept: application/json' \
    --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ3MTUzMzAwNH0.aDJt7ETyizI6paesQmcBcVXJi6a_VfQfh6uuBKY7024fjKHOFVshL8NsNvYwywPdoaYzo3sCnc0doUAh3k76RQ' \
    -d '{
    "uid": "gaturro3",
    "code": "comic.gaturro3",
    "description": "Comic de Gaturro Nro 3",
    "title": "El Mundo de Gaturro Diciembre",
    "safeTitle": "mundo-gaturro-3",
    "number": "0003",
    "imageSrc": "http://www.todohistorietas.com.ar/tiragaturro.gif"
    }' 'http://127.0.0.1:8070/api/comics'
echo "\ndone!\n";
