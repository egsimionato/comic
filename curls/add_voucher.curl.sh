#export RAND=rand;
curl -X POST --header 'Content-Type: application/json' \
    --header 'Accept: application/json' \
    -d "{
    \"uid\": \"comic.voucher.$RAND\",
    \"type\": \"FREE\",
    \"createdAt\": \"2016-08-16T11:27:42.787-03:00\",
    \"updatedAt\": \"2016-08-16T11:27:42.787-03:00\",
    \"status\": \"NEW\",
    \"accountId\": $ACCOUNT_ID,
    \"accountUsername\": \"$USERNAME\"
}" \
"http://$COMIC_SERVER/api/vouchers"
