Paso a Paso Comic Server
========================

# GET TOKEN
-----------
``
    > curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
        "password": "admin",
        "rememberMe": true,
        "username": "admin"
        }' 'http://p8081.msdev.cmd.com.ar/api/authenticate'
``
`` #RETORNO 
    {
        "id_token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTQ3MTYzMzU0Mn0.BIPtdvfCWFaFgFhEsyaL0ArO9UyngpPrKzuh5c_D4KhX4ENr9FM8nh0i_-pt2lqT44UIEcaDI-LM476OajcgtA"
    }
``


# GET ACCOUNT_INFO [<token>|<username>]
------------------
``
    > curl -X GET --header 'Accept: application/json'\
        --header 'Authorization: Bearer <token>' \
       'http://p8082.msdev.cmd.com.ar/api/account_info/<username>'
      # 'http://p8082.msdev.cmd.com.ar/api/me/<username}/info'
``

# GET COMICS
------------
``
    > curl -X GET --header 'Accept: application/json'\
        --header 'Authorization: Bearer <token>' \
        'http://p8082.msdev.cmd.com.ar/api/comics'
``

# POST COMICS
--------------
``
    > curl -X POST --header 'Content-Type: application/json' \
        --header 'Accept: application/json' \
        --header 'Authorization: Bearer <token>' \
        -d '{
        "uid": "comic.gaturro1",
        "code": "comic.gaturro1",
        "description": "Comic de Gaturro Nro 1",
        "title": "El Mundo de Gaturro Octubre",
        "safeTitle": "mundo-gaturro-1",
        "number": "0001",
        "image_src": "http://images.ucomics.com/images/uclick/spgat/spgat040605.gif"
        }' 'http://p8082.msdev.cmd.com.ar/api/comics'

``


# BUY VAUCHER
-------------
``
    > curl -X POST --header 'Content-Type: application/json' \
        --header 'Accept: application/json' \
        --header 'Authorization: Bearer <token>' \
        -d '{
            "username": "<username>"
        }' 'http://p8082.msdev.cmd.com.ar/api/buy_voucher'
      # }' 'http://p8082.msdev.cmd.com.ar/api/me/{username}/buy_voucher'
``
`` # CON 200
   # CON 400 por saldo insuficiente
``


# FREE VAUCHER
--------------
``
    > curl -X POST --header 'Content-Type: application/json' \
        --header 'Accept: application/json' \
        --header 'Authorization: Bearer <token>' \
        -d '{
            "username": "<username>"
        }' 'http://p8082.msdev.cmd.com.ar/api/free_voucher'
      # }' 'http://p8082.msdev.cmd.com.ar/api/me/{username}/free_voucher'
``
`` # CON 200
   # CON 400 por time no cumplido
``


# UNLOCK A COMIC
----------------
``
    > curl -X POST --header 'Content-Type: application/json' \
        --header 'Accept: application/json' \
        --header 'Authorization: Bearer <token>' \
        -d '{
            "username": "matildita",
            "comicCode": "comic.gaturro1"
        }' 'http://p8082.msdev.cmd.com.ar/api/unlock_comic'
      # }' 'http://p8082.msdev.cmd.com.ar/api/me/{username}/unlock_comic/{comic_id}'
``



# UNLOCK RANDOM
---------------
``
    > curl -X POST --header 'Content-Type: application/json' \
        --header 'Accept: application/json' \
        --header 'Authorization: Bearer <token>' \
        -d '{
            "username": "matildita",
        }' 'http://p8082.msdev.cmd.com.ar/api/unlock_random'
      # }' 'http://p8082.msdev.cmd.com.ar/api/me/{username}/unlock_random'
``


# GET DIGITAL_COPIES
--------------------
``
    > curl -X GET --header 'Accept: application/json'\
        --header 'Authorization: Bearer <token>' \
        'http://p8082.msdev.cmd.com.ar/api/digital-copies'
       # 'http://p8082.msdev.cmd.com.ar/api/me/{userame}/digital-copies'
``

