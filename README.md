Paso a Paso Comic Server
========================
``
PARA SABER:
  > v2/api/
     -> /authenticate 
     -> /reward/*
    +-> /reward/me/{username}/*
     -> /comic/*
    +-> /comic/me/{username}/*

  > componetes : 8080-8090 -> p8080.msdev.cmd.com.ar-p8090.msdev.cmd.com.ar
    + ngix: 80         -> msdev.cmd.com.ar/v2/*
    + zull: 8080       -> p8080.msdev.cmd.com.ar
    + ida: 8081        -> p8081.msdev.cmd.com.ar
    + comic: 8082      -> p8082.msdev.cmd.com.ar
    + reward: 8083     -> p8083.msdev.cmd.com.ar
``
-----------------------------------------------------------------------------------------

ACCOUNT: [ BUY | FREE | INFO | UNLOCK ]
----------------------------------------
``
     [GET] api/collector-account/{username}
       |-> 200 { id, uudi, username, email, new_vouchers_count: 999  }
     [GET] api/collector-account/{username}/voucher-count/status/{new}
       |-> { vauchers_count: 999  }
     [GET] api/collector-account/{username}/voucher-free-elapsed-time
       |-> { voucher-free-elapsed-time: 00:00  }
     [GET] api/collector-accounts/{username}/voucher-price
       |-> 200 { voucher-price: 50  }
     [POST] api/collector-accounts/{username}/free_voucher?freeWindowTimeInSeconds=00000 |
       |-> 200 y redirect to [GET] api/comic/collector-account/{username}/voucher-count/status/{new} { vauchers_count: 999  }
       |-> 400 { error: "Daily Free Voucher Used", proximo_time : "00:00:10"  }
     [POST] api/collector-accounts/{username}/buy_voucher
       |-> 200 y redirect to [GET] api/comic/collector-account/{username}/voucher-count/status/{new} { vauchers_count: 999  }
       |-> 400 { error: "El usuario no tiene plata", voucher-price: 50, user-amount: 20  }
     [POST] api/collector-accounts/{username}/unlock-comic/{comic_id}|{comic-code}
``

COMICS [ LIST ALL ] | COPIES [ By USERNAME ]
-------------------------------------------------
``
    [GET] api/comic/comics
      |-> 200 { id, uui, code, title, description, imageSrc  }
    [GET] api/comic/digital-copies/username/{username}
      |-> 200 { id, username, comic_id, createdAt  }
``

